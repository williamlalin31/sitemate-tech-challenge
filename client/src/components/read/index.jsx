import React, { useState } from 'react';

const ReadModule = () => {
  const [displayData, setDisplayData] = useState();
  const handleRead = async () => {
    const response = await fetch("http://127.0.0.1:4040/read");
    if (response) {
      const data = await response.json();
      setDisplayData(data);
    }
  }

  return <div className="module-panels">
    <p>This will call the BE API and display the result</p>
    <button onClick={handleRead} style={{ marginBottom: 8 }}>Read Data</button>
    {
      displayData && 
      <div>
        <div>Result:</div>
        <div>
          {JSON.stringify(displayData)}
        </div>
      </div>
    }
  </div>
}

export default ReadModule;