const CreateData = () => {
  const handlePost = async () => {
    const reqOptions = {
      method: "POST",
      body: JSON.stringify({
        id: "1",
        title: "Posted Data",
        description: "This is an POSTed data"
      })
    }

    const response = await fetch("http://127.0.0.1:4040/create", reqOptions);
    const resData = await response.json();
    console.log(JSON.stringify(resData));
  }

  return <div className="module-panels">
    <p>This will call the BE API, pass an issue, and the server will log the result</p>
    <button onClick={handlePost}>Create Data</button>
  </div>
}

export default CreateData;