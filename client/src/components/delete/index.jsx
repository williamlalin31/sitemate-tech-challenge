const DeleteData = () => {
  const handleUpdate = async () => {
    const reqOptions = { method: "DELETE" }

    const response = await fetch("http://127.0.0.1:4040/delete?id=1", reqOptions);
    const resData = await response.json();
    console.log(JSON.stringify(resData));
  }

  return <div className="module-panels">
    <p>This will call the BE API, delete the data, and the server should print the deleted record ID</p>
    <button onClick={handleUpdate}>Delete Data</button>
  </div>
}

export default DeleteData;