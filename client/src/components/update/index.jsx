const UpdateData = () => {
  const handleUpdate = async () => {
    const reqOptions = {
      method: "PUT",
      body: JSON.stringify({
        id: "1",
        title: "Updated Data",
        description: "This is an Updated data"
      })
    }

    const response = await fetch("http://127.0.0.1:4040/update", reqOptions);
    const resData = await response.json();
    console.log(JSON.stringify(resData));
  }

  return <div className="module-panels">
    <p>This will call the BE API, update an issue, and the server will log the result</p>
    <button onClick={handleUpdate}>Update Data</button>
  </div>
}

export default UpdateData;