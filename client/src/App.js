import './App.css';
import CreateData from './components/create';
import DeleteData from './components/delete';
import ReadModule from './components/read';
import UpdateData from './components/update';

function App() {
  return (
    <div className="App">
      <ReadModule />
      <CreateData />
      <UpdateData />
      <DeleteData />
    </div>
  );
}

export default App;
