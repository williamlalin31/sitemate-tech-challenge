# sitemate-tech-challenge

## Server
Used nodejs
To run the server, go to /server dir, then run "node index.js"
server should boot up at localhost:4040

## Client
Used react
To run the client, go to /client dir, then run command "npm start"
client should boot up at localhost:3000