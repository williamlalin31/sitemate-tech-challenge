const http = require("http");

const hostname = '127.0.0.1';
const port = 4040;

const server = http.createServer();

server.on("request", (req, res) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'OPTIONS, POST, GET, DELETE, PUT',
    'Access-Control-Max-Age': 2592000,
    'Content-Type': 'application/json'
  };

  if (req.method === 'OPTIONS') {
    res.writeHead(204, headers);
    res.end();
    return;
  }

  if (req.method === "POST") {
    if (req.url.startsWith("/create")) {
      const dataChunks = [];

      req
        .on("data", (chunk) => {
          dataChunks.push(chunk);
        })
        .on("end", () => {
          const stringBuffer = Buffer.concat(dataChunks).toString();
          const actualData = JSON.parse(stringBuffer);

          console.log(actualData);

          res.writeHead(201, headers);
          res.write(JSON.stringify(actualData));
          res.end();
        })
    }
  }

  if (req.method === "PUT") {
    if (req.url.startsWith("/update")) {
      const dataChunks = [];

      req
        .on("data", (chunk) => {
          dataChunks.push(chunk);
        })
        .on("end", () => {
          const stringBuffer = Buffer.concat(dataChunks).toString();
          const actualData = JSON.parse(stringBuffer);

          console.log(actualData);

          res.writeHead(200, headers);
          res.write(JSON.stringify(actualData));
          res.end();
        })
    }
  }

  if (req.method === "GET") {
    if (req.url.startsWith("/read")) {
      const responseBody = {
        id: 1,
        title: "Data 1",
        description: "First data entry"
      }

      res.writeHead(200, headers);
      res.write(JSON.stringify(responseBody));
      res.end();
    }
  }

  if (req.method === "DELETE") {
    if (req.url.startsWith("/delete")) {

      const urlSplit = req.url.split("?id=");
      const idParam = urlSplit[1];

      res.writeHead(200, headers);
      res.write(JSON.stringify({ id: idParam }));
      res.end();
    }
  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}`)
});